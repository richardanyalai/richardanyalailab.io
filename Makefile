build:
	@cd donut && wasm-pack build --target web
	@rm -rf public/wasm
	@cp -r donut/pkg public/wasm

host:
	@npx live-server public
