import init, { donut } from "../wasm/donut.js"

let a = 0
let b = 0

const update = () => {
  donut(a, b)

  a += 0.04
  b += 0.02
}

init().then(() => {
  setInterval(update, 25)
})
