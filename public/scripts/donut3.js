const scene = new THREE.Scene()
const camera = new THREE.PerspectiveCamera(70, 1, 0.2, 100)

camera.position.x = 0.7
camera.position.y = -1.5
camera.position.z = 5.2

const renderer = new THREE.WebGLRenderer({
  alpha: true,
  antialias: true,
})

renderer.setPixelRatio(window.devicePixelRatio)
renderer.shadowMap.enabled = true
renderer.domElement.id = "canvas"
document.body.appendChild(renderer.domElement)

const canvas = document.getElementById("canvas")
canvas.style.display = "none"

renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)

const dracoLoader = new THREE.DRACOLoader()
dracoLoader.setDecoderPath("https://threejs.org/examples/js/libs/draco/gltf/")

const gltfLoader = new THREE.GLTFLoader()
gltfLoader.setDRACOLoader(dracoLoader)

gltfLoader.load("./donut.glb", (gltf) => {
  const torus = gltf.scene

  torus.scale.set(1, 1, 1)
  scene.add(torus)

  const pointLight = new THREE.PointLight(0xffffff, 1, 50)
  pointLight.position.set(0, 0, 10)
  scene.add(pointLight)

  torus.rotation.x = -80

  const animate = () => {
    requestAnimationFrame(animate)

    render()
  }

  const render = () => {
    if (canvas.style.display !== "none") {
      canvas.style.width = canvas.style.height

      torus.rotation.z += 0.04
      torus.rotation.x += 0.02

      renderer.render(scene, camera)
    }
  }

  animate()
})
