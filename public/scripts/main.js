const donut = document.getElementById("donut")

let is3D = true

const clickHandler = () => {
  if (is3D) {
    donut.style.display = "none"
    canvas.style.height = "900px"
    canvas.style.display = "block"
  } else {
    canvas.style.display = "none"
    donut.style.display = "block"
  }

  is3D = !is3D
}

donut.addEventListener("click", clickHandler)
canvas.addEventListener("click", clickHandler)

console.log("Hello friend")
